import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Message from '@/components/chap01/Message'
import Click from '@/components/chap01/Click'
import Form from '@/components/chap01/Form'
import List from '@/components/chap01/List'
import IfElse from '@/components/chap01/IfElse'
import Transition from '@/components/chap01/Transition'
import NestData from '@/components/chap02/NestData'
import Increment from '@/components/chap02/Increment'
import ClassStyleBind from '@/components/chap02/ClassStyleBind'

Vue.use(Router)

export default new Router({
  routes: [
    { path: '/', name: 'HelloWorld', component: HelloWorld },
    { path: '/ch01/text', name: 'TextBind', component: Message },
    { path: '/ch01/list', name: 'List', component: List },
    { path: '/ch01/click', name: 'Click', component: Click },
    { path: '/ch01/form', name: 'Form', component: Form },
    { path: '/ch01/ifElse', name: 'IfElse', component: IfElse },
    { path: '/ch01/transition', name: 'Transition', component: Transition },
    // chapter2
    { path: '/ch02/nestdata', name: 'NestData', component: NestData },
    { path: '/ch02/increment', name: 'Increment', component: Increment },
    { path: '/ch02/class-style-bind', name: 'ClassStyleBind', component: ClassStyleBind }
  ]
})
