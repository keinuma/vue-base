FROM node:8.11.1

ENV HOST=0.0.0.0
ENV PORT=8000

WORKDIR /var/vuejs/
COPY package.json .
RUN npm install
ADD . /var/vuejs/

VOLUME $PWD:/var/vuejs/
CMD npm run dev
